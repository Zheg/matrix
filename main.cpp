//
// Created by lesHa on 19/03/2020.
//

#include "Matrix.h"
#include <iostream>
#include <iomanip>

using namespace std;

int main(){
    Matrix a;
    a.resize(3, 3);
    for (int i = 0; i < a.rowCount(); ++i){
        for (int j = 0; j < a.colCount(); ++j){
            a.at(i, j) = i + j;
        }
    }
    cout << "Matrix a:" << endl;
    for (int i = 0; i < a.rowCount(); ++i){
        for (int j = 0; j < a.colCount(); ++j){
            cout << setw(4) << a.at(i, j) << " ";
        }
        cout << endl;
    }

    Matrix b;
    b.resize(3, 6);
    for (int i = 0; i < b.rowCount(); ++i){
        for (int j = 0; j < b.colCount(); ++j){
            b.at(i, j) = i + j;
        }
    }
    cout << "Matrix b:" << endl;
    for (int i = 0; i < b.rowCount(); ++i){
        for (int j = 0; j < b.colCount(); ++j){
            cout << setw(4) << b.at(i, j) << " ";
        }
        cout << endl;
    }

    Matrix multiplied;
    multiplied = a * b;
    cout << "Matrix multiplied (a * b):" << endl;
    for (int i = 0; i < multiplied.rowCount(); ++i){
        for (int j = 0; j < multiplied.colCount(); ++j){
            cout << setw(4) << multiplied.at(i, j) << " ";
        }
        cout << endl;
    }

    Matrix added;
    added = b + multiplied;
    cout << "Matrix added (b + multiplied):" << endl;
    for (int i = 0; i < added.rowCount(); ++i){
        for (int j = 0; j < added.colCount(); ++j){
            cout << setw(4) << added.at(i, j) << " ";
        }
        cout << endl;
    }
}