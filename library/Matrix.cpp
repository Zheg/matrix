#include "Matrix.h"
#include <stdexcept>

Matrix::Matrix() {
    matrix = new double *[0];
    matrix[0] = new double[0];
    matrix[0][0] = 0;
    rows = 1;
    columns = 1;
}

Matrix::Matrix(double** newMatrix, int rows, int columns) {
    matrix = new double*[rows];
    for (int i = 0; i < rows; ++i){
        matrix[i] = new double[columns];
    }
    for (int i = 0; i < rows; ++i){
        for (int j = 0; j < columns; ++j){
            matrix[i][j] = newMatrix[i][j];
        }
    }
    this->rows = rows;
    this->columns = columns;
}

Matrix::~Matrix(){
    for (int i = 0; i < rows; ++i){
        delete[] matrix[i];
    }
    delete[] matrix;
}

int Matrix::colCount() const {
    return this->columns;
}

int Matrix::rowCount() const {
    return this->rows;
}

//double **Matrix::getMatrix() const {
//    return this->matrix;
//}

double Matrix::at(int i, int j) const {
    if (i >= rows || i < 0 || j >= columns || j < 0)
        throw std::overflow_error("Index out of range");
    return matrix[i][j];
}

void Matrix::resize(int newRows, int newColumns, double newEl) {
    double **tmp = new double *[newRows];
    for (int i = 0; i < newRows; ++i) {
        tmp[i] = new double[newColumns];
    }
    for (int i = 0; i < newRows; ++i) {
        for (int j = 0; j < newColumns; ++j) {
            if (i < rows && j < columns) {
                tmp[i][j] = matrix[i][j];
            } else {
                tmp[i][j] = newEl;
            }
        }
    }
    matrix = tmp;
    rows = newRows;
    columns = newColumns;
    return;
}

void Matrix::clear() {
    for (int i = 0; i < rows; ++i) {
        for (int j = 0; j < columns; ++j) {
            matrix[i][j] = 0;
        }
    }
    return;
}

Matrix& Matrix::operator=(const Matrix &mt) {
    rows = mt.rowCount();
    columns = mt.colCount();
    matrix = new double*[rows];
    for (int i = 0; i < rows; ++i){
        matrix[i] = new double[columns];
    }
    for (int i = 0; i < rows; ++i){
        for (int j = 0; j < columns; ++j){
            matrix[i][j] = mt.at(i, j);
        }
    }
    return *this;
}

double *&Matrix::operator[](int i) {
    return matrix[i];
}

double &Matrix::at(int i, int j) {
    if (i >= rows || i < 0 || j >= columns || j < 0)
        throw std::overflow_error("Index out of range");
    return matrix[i][j];
}


Matrix operator+(const Matrix &lhs, const Matrix &rhs) {
    if (lhs.colCount() != rhs.colCount() || lhs.rowCount() != rhs.rowCount())
        throw std::runtime_error("Diffent size of matrix");
    int n = lhs.rowCount();
    int m = lhs.colCount();
    double **tmp = new double *[n];
    for (int i = 0; i < n; ++i) {
        tmp[i] = new double[m];
    }
    for (int i = 0; i < n; ++i) {
        for (int j = 0; j < m; ++j) {
            tmp[i][j] = lhs.at(i, j) + rhs.at(i, j);
        }
    }
    return {tmp, n, m};
}

Matrix operator*(const Matrix &lhs, const Matrix &rhs) {
    if (lhs.colCount() != rhs.rowCount())
        throw std::runtime_error("LHS rows not equal RHS columns");
    double **tmp;
    int n = lhs.rowCount();
    int m = rhs.colCount();
    tmp = new double *[n];
    for (int i = 0; i < n; ++i) {
        tmp[i] = new double[m];
    }
    double el = 0;
    for (int i = 0; i < n; ++i) {
        for (int j = 0; j < m; ++j) {
            el = 0;
            for (int k = 0; k < lhs.colCount(); ++k) {
                el += lhs.at(i, k) * rhs.at(k, j);
            }
            tmp[i][j] = el;
        }
    }
    return {tmp, n, m};
}