#ifndef MATRIXC_MATRIX_H
#define MATRIXC_MATRIX_H

class Matrix {
public:
    Matrix();

    Matrix(double **newMatrix, int rows, int columns);

    ~Matrix();

    int rowCount() const;

    int colCount() const;

//    double **getMatrix() const;

    double at(int i, int j) const;

    double& at(int i, int j);

    void resize(int newRows, int newColumns, double newEl = 0);

    void clear();

    Matrix& operator=(const Matrix &mt);

    double*& operator[](int i);


private:
    double **matrix;
    int rows;
    int columns;
};

Matrix operator+(const Matrix &lhs, const Matrix &rhs);

Matrix operator*(const Matrix &lhs, const Matrix &rhs);


#endif //MATRIXC_MATRIX_H