cmake_minimum_required(VERSION 3.14)
project(MatrixC)

set(CMAKE_CXX_STANDARD 14)

add_subdirectory(library)

add_executable(MatrixC main.cpp)

include_directories(library)

target_link_libraries(MatrixC ${CMAKE_SOURCE_DIR}/library/Matrix.h ${CMAKE_SOURCE_DIR}/library/Matrix.cpp)
#target_link_libraries(MatrixC ${CMAKE_SOURCE_DIR}/cmake-build-debug/library/liblibrary)